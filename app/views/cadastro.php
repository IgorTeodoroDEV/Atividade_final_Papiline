<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    <link rel="stylesheet" href="../public/css/login.css">
    <title>Cadastro</title>
</head>
<body>
    <main>
        <h1>Cadastro</h1>
        <form action="../controllers/inserir.php" method="post">
            <label for="usuario">usuario</label>
            <input type="text" id="usuario" name="usuario">
            <label for="senha">Senha</label>
            <input type="password" id="senha" name="senha">
            <input type="submit" class="btn" value="Fazer cadastro">
        </form>
    </main>
</body>
</html>